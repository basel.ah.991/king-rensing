<?php

namespace App\Http\Controllers;

use App\Mail\ContactMail;
use App\Models\Comment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class CommentController extends FacebookController
{
    /**
     * Display a listing of the resource.
     */
    public function index()
    {

        $this->getComments();

        $comments = Comment::where('show',true)->get();

        return response()->json( ['comments'=> $comments]);

    }

    /**
     * Store a newly created resource in storage.
     */
    public function sendEmail(Request $request)
    {
        //kidoo.012.12@gmail.com
        $to_email = 'kidoo.012.12@gmail.com';

        Mail::to($to_email)->send(new ContactMail($request->name,$request->email,$request->subject,$request->text,$request->phone));

        return response()->json(['message'=> 'email sent success' ]);
    }

    /**
     * Display the specified resource.
     */
    public function show(string $id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     */
    public function update(Request $request, string $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     */
    public function destroy(string $id)
    {
        //
    }
}
