<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use Facebook\Facebook;
use GuzzleHttp\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class FacebookController extends Controller
{

    private $appId="315506131545301";
    private $appSecret="3b9cc3495cb9f62a56d834c05edef92d";
    private $shortLivedToken;

   private function refreshFacebookToken($appId, $appSecret, $shortLivedToken) {
    $client = new Client();

    $response = $client->get("https://graph.facebook.com/oauth/access_token", [
        'query' => [
            'grant_type' => 'fb_exchange_token',
            'client_id' => $appId,
            'client_secret' => $appSecret,
            'fb_exchange_token' => $shortLivedToken
        ]
    ]);

    return json_decode($response->getBody(), true)['access_token'];
}
    public function getComments()
    {
        // Initialize Facebook SDK
        $fb = new Facebook([
            'app_id' => $this->appId,
            'app_secret' => $this->appSecret,
            'default_graph_version' => 'v19.0', // Change this to the latest version
        ]);

        // Access token with required permissions

        try {
            // Specify the post ID from which you want to retrieve comments
            $pageId = '151980364658655';
            $tokens = DB::table('tokens')->first();
            $this->shortLivedToken=$tokens->token;
            $refreshedToken = $this->refreshFacebookToken($this->appId, $this->appSecret, $this->shortLivedToken);
            $this->shortLivedToken = $refreshedToken;
            DB::table('tokens')->update(['token'=>$refreshedToken]);
            // Request to retrieve comments
            $response = $fb->get("/{$pageId}/posts?fields=id,message,comments{id,message,from{name}}", $refreshedToken);

            //  dd($response->getGraphNode());
            // Get GraphEdge object containing posts
            $postsEdge = $response->getGraphEdge();

            // Convert GraphEdge to array
            $posts = $postsEdge->asArray();


            foreach ($posts as $post) {
                if (isset($post['comments'])) {
                    foreach ($post['comments'] as $comment) {
                        if (isset($comment['from'])) {
                            $userName = $comment['from']['name'];
                        } else {
                            $userName = "Guest"; // or any default value you prefer
                        }
                        $commentt = Comment::where('comment_id', $comment['id'])->first();
                        if (!$commentt) {
                            Comment::create([
                                'content' => $comment['message'],
                                'type' => 'facebook',
                                'user' =>  $userName,
                                'comment_id' => $comment['id']

                            ]);
                        }
                    }
                }
            }



            return response()->json($posts);
        } catch (\Facebook\Exceptions\FacebookResponseException $e) {
            // Handle error
            return response()->json(['error' => $e->getMessage()], 500);
        } catch (\Facebook\Exceptions\FacebookSDKException $e) {
            // Handle error
            return response()->json(['error' => $e->getMessage()], 500);
        }
    }
}
