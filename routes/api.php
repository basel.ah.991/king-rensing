<?php

use App\Http\Controllers\CommentController;
use App\Http\Controllers\FacebookController;
use App\Http\Controllers\ImageController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::get('/images', [ImageController::class, 'index']);
Route::get('/comments', [CommentController::class, 'index']);
Route::post('/send-email', [CommentController::class, 'sendEmail']);
Route::get('/facebook/comments', [FacebookController::class, 'getComments']);
Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});
