<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>Kontakt Email</title>
</head>

<body>
    <p>name {{ $name }}</p>
    <p>email {{ $email }}</p>
    <p>phone {{ $phone }}</p>
    <p>message: </p>
    <p>{{ $text }}</p>
</body>

</html>
